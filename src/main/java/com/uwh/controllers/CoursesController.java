package com.uwh.controllers;

import static org.springframework.http.HttpStatus.OK;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.api.request.CourseUpdateDto;
import com.uwh.api.request.StudentIds;
import com.uwh.api.response.CoursesDto;
import com.uwh.service.CourseService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

/**
 * Organization controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "Courses resource")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH + "/course")
public class CoursesController {

	@Autowired
	private CourseService courseService;
	
	@GetMapping("/all/partner/{partnerId}")
	@ApiOperation("This api is to get the list of courses")
	public ResponseEntity<List<CoursesDto>> findCoursesByPartner(@PathVariable Long partnerId) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(courseService.findAllCoursesOfPartner(partnerId), OK);
	}
	
	@GetMapping("/all")
	@ApiOperation("This api is to get the list of courses")
	public ResponseEntity<List<CoursesDto>> findAllCourses() throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(courseService.findAllCourses(), OK);
	}
	
	@PostMapping("/save")
	@ApiOperation("This api is to get the add course")
	public ResponseEntity<CoursesDto> saveCourse(@RequestBody CoursesDto coursesDto) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(courseService.saveCourse(coursesDto), OK);
	}
	
	@PostMapping("/enrollStudent/{courseId}")
	@ApiOperation("This api is to enroll students")
	public ResponseEntity<CoursesDto> enrollStudentToCourse(@PathVariable Long courseId, @RequestBody StudentIds studentIds) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(courseService.enrollStudentToCourse(courseId, studentIds.getStudentIds()), OK);
	}
	
	@PutMapping("/update")
	@ApiOperation("This api is to get the update course")
	public ResponseEntity<CoursesDto> updateCourse(@RequestBody CourseUpdateDto coursesDto) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(courseService.updateCourse(coursesDto), OK);
	}
}
