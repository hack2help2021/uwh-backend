package com.uwh.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.api.request.LoginRequestDto;
import com.uwh.domain.Student;
import com.uwh.service.StudentService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;
/**
 * 
 * @author abdus
 * This Controller is used to Login Student.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "UWH Backend APIs")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH)
public class LoginController {
	
	@Autowired
	private StudentService studentService;
	
	@PostMapping("/login")
	public ResponseEntity<Optional<Student>> authenticateUser(@RequestBody LoginRequestDto loginRequestDto) {
		// model.addAttribute("product", productService.getProductById(id));
	
		String role = loginRequestDto.getRole();//student or Admin
		
		String uid = loginRequestDto.getUid();	// user id
		
		String uidType = loginRequestDto.getUidType(); // Aadhar/PAN/MobNo
		
		String password = loginRequestDto.getPassword();
		
		
		return new ResponseEntity<>(studentService.findByRoleAndUidAndUidTypeAndPassword(role,uid,uidType,password),HttpStatus.OK);
	}
	
	
}
