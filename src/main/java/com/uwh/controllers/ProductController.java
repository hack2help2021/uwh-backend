/*
 * package com.uwh.controllers;
 * 
 * import java.util.List; import java.util.Optional;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.ui.Model; import
 * org.springframework.web.bind.annotation.CrossOrigin; import
 * org.springframework.web.bind.annotation.GetMapping; import
 * org.springframework.web.bind.annotation.PathVariable; import
 * org.springframework.web.bind.annotation.PutMapping; import
 * org.springframework.web.bind.annotation.RequestBody; import
 * org.springframework.web.bind.annotation.RequestMapping; import
 * org.springframework.web.bind.annotation.RequestMethod; import
 * org.springframework.web.bind.annotation.RestController;
 * 
 * import com.uwh.domain.Product; import com.uwh.service.ProductService; import
 * com.uwh.utils.Constant;
 * 
 * import io.swagger.annotations.Api;
 * 
 *//**
	 * Product controller.
	 */
/*
 * @CrossOrigin(origins = "*", maxAge = 3600)
 * 
 * @Api(value = "UWH Backend APIs")
 * 
 * @RestController
 * 
 * @RequestMapping(value = Constant.BASE_CONTEXT_PATH) public class
 * ProductController {
 * 
 * private ProductService productService;
 * 
 * @Autowired public void setProductService(ProductService productService) {
 * this.productService = productService; }
 * 
 *//**
	 * List all products.
	 *
	 * @param model
	 * @return
	 */
/*
 * @GetMapping("/products") public List<Product> getProducts() { List<Product>
 * products = productService.listAllProducts();
 * System.out.println("Returning rpoducts:"); return products; }
 * 
 *//**
	 * View a specific product by its id.
	 *
	 * @param id
	 * @param model
	 * @return
	 */
/*
 * @GetMapping("product/{id}") public Optional<Product>
 * getProductById(@PathVariable Integer id, Model model) { //
 * model.addAttribute("product", productService.getProductById(id)); return
 * productService.getProductById(id); }
 * 
 * // Afficher le formulaire de modification du Product
 * // @RequestMapping("product/edit/{id}")
 * 
 * @PutMapping(value = "/product") public Product updateProduct(@RequestBody
 * Product product) {
 * 
 * return productService.saveProduct(product); }
 * 
 *//**
	 * New product.
	 *
	 * @param model
	 * @return
	 */
/*
 * @RequestMapping("product/new") public String newProduct(Model model) {
 * model.addAttribute("product", new Product()); return "productform"; }
 * 
 *//**
	 * Save product to database.
	 *
	 * @param product
	 * @return
	 */
/*
 * @RequestMapping(value = "product", method = RequestMethod.POST) public String
 * saveProduct(Product product) { productService.saveProduct(product); return
 * "redirect:/product/" + product.getId(); }
 * 
 *//**
	 * Delete product by its id.
	 *
	 * @param id
	 * @return
	 *//*
		 * @RequestMapping("product/delete/{id}") public String delete(@PathVariable
		 * Integer id) { productService.deleteProduct(id); return "redirect:/products";
		 * }
		 * 
		 * }
		 */