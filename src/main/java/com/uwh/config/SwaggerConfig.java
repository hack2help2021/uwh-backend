package com.uwh.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.uwh.utils.Constant;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

//@EnableSwagger2
@Configuration
public class SwaggerConfig {
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build();                                           
    }
	
	
	  @Bean public Docket documentation() { return new
	  Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false).select(
	  ) .apis(RequestHandlerSelectors.basePackage("com.uwh.controllers"))
	  .paths(PathSelectors.any()).build().pathMapping("/").apiInfo(metadata()).
	  groupName("uwh-backend"); }
	  
	  private ApiInfo metadata() { return new
	  ApiInfoBuilder().title("Crimatrix Service API").
	  description("UWH Backend Service API Description")
	  .version(Constant.API_VERSION).license("Copyright Happy2Help.com").licenseUrl
	  ("https://www.localhost:9000")
	  .termsOfServiceUrl("https://www.wekare.com").build(); }
}
