package com.uwh.domain;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Table(name = "job")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String jobType;//full-time/part-time
    private String jobName;// what could be the job Name?
    private String jobRole;//Typist/FrontDeskManager
    private String JobDescription;// Description about Job Duties, roles , day to day task.
    private String jobLocation; //HYD/Chennai
    private String jobIndustry;//HealthCare/Corporate/Repearing/jewellery/Teaching    
    private String minimumQualification;//Diploma/BSc/BA/BEd
    private String additionalQualification;//BEd,Vocational etc.
    private Double minSalary;
    private Double maxSalary;
    private Boolean travelRequired;
    private String jobPostingDate;    
    private String jobPostedByName;
    private String jobPostedByEmailId;
    private String jobPostedByMobileNo;
         
    private String jobValidateTillEndDate;
    private Integer jobOpenVacancy;
    
    @OneToMany(mappedBy = "job", targetEntity = JobSkills.class,cascade = CascadeType.ALL,orphanRemoval = true)
    private List<JobSkills> skills; 
    
    
    //why do we need to have course in Job??
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Courses courses; 
    
    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private List<StudentJobDetails> students;
}
