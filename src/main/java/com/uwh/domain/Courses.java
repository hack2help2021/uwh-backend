package com.uwh.domain;



import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ramakrishna Veldandi
 * @project UWH
 */

@Entity
@Table(name = "courses")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Courses{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String courseName;
    private String skills;
    private Integer duration;
    private Integer numberOfSeats; //30
    private Integer numberOfEnrollment; //25
    private Integer numberOf; //25
    private String details;
    private String fileLocation;
    
    private String startDate;
    private String endDate;
    
    @ManyToOne
    @JoinColumn(name = "partner_id")
    private Partners partners;
    
    @JsonIgnore
    @OneToMany(mappedBy = "courses")
    private List<StudentCourse> studentCourse;
}
