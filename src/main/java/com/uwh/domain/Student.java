package com.uwh.domain;



import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ramakrishna Veldandi
 * @project UWH
 * 
 */

@Entity
@Table(name = "student")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "org_id")
    private Organization organization;
    private String uid;
    private String uidType;
    
    private String password;
    private String role;
    
    
    private String regdNo;
    private String studentName;
    private String gender;
    private String phone;
    private String additionalPhone;
    private String bplCard;
   // private Timestamp regdDate;
    private String regdDate;
    private String purpose;//Dropdown: College Placement Assistance/Hunar Course And Job/
    
    //private Timestamp dateOfBirth;
    private String dateOfBirth;
    
    private String qualification;
    private String additionalCourse;
    private String university;
    private Integer experience;
    private String studentCurrentJobFields;
    private Double currentSalary;
    private String studentCurrentSkills;
    private String studentInterestedSkills;
    private String studentInterestedJobFields;
    private String studentJobLocationPreferences;
        
    private Double expectedJobSalary;
    
    private String currentAddress;
    private String currentCity;
    private String districtName;
    private String pinCode;
            
    private String email;
    private String phone1;
    private String whatsAppNo2;
    
    private String referredByType; //drop down -> Self, Sponsored By - Honur/Colleague/FB/Friends/Other/Marketing Channel
    private String referredByName;
    
    @JsonIgnore
    @OneToMany(mappedBy = "student")
    private List<StudentCourse> courses; 
    
    @JsonIgnore
    @OneToMany(mappedBy = "student")
    private List<StudentJobDetails> jobs; 
}
