package com.uwh.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.uwh.api.response.BatchesDTO;
import com.uwh.domain.Batches;
import com.uwh.domain.Courses;
import com.uwh.domain.Student;
import com.uwh.repositories.CountStudentRepository;

@Repository
public interface CountStudentService {
	
 int countStudents() throws IllegalAccessException, InvocationTargetException;
	}


