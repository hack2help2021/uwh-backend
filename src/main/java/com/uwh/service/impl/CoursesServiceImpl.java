package com.uwh.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uwh.api.request.CourseUpdateDto;
import com.uwh.api.response.CoursesDto;
import com.uwh.domain.Courses;
import com.uwh.domain.Student;
import com.uwh.domain.StudentCourse;
import com.uwh.exceptions.BadRequestException;
import com.uwh.repositories.CoursesRepository;
import com.uwh.repositories.PartnersRepository;
import com.uwh.repositories.StudentCourseRepository;
import com.uwh.repositories.StudentRepository;
import com.uwh.service.CourseService;
import com.uwh.utils.DtoConverter;

/**
 * Product service implement.
 */
@Service
@Transactional
public class CoursesServiceImpl implements CourseService {

	@Autowired
	private CoursesRepository coursesRepository;

	@Autowired
	private PartnersRepository partnersRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private StudentCourseRepository studentCourseRepository;

	@Override
	public List<CoursesDto> findAllCoursesOfPartner(Long partnerId)
			throws IllegalAccessException, InvocationTargetException {
		return DtoConverter.convertToCoursesDto(coursesRepository.findAllByPartnersId(partnerId));
	}

	@Override
	public CoursesDto saveCourse(CoursesDto coursesDto) throws IllegalAccessException, InvocationTargetException {
		Courses courses = new Courses();
		BeanUtils.copyProperties(coursesDto, courses);
		courses.setPartners(partnersRepository.findById(coursesDto.getPartners().getId()).get());
		courses = coursesRepository.save(courses);
		coursesDto.setId(courses.getId());
		return coursesDto;
	}

	@Override
	public List<CoursesDto> findAllCourses() throws IllegalAccessException, InvocationTargetException {
		return DtoConverter.convertToCoursesDto(coursesRepository.findAll());
	}

	@Override
	public CoursesDto enrollStudentToCourse(Long courseId, List<Long> studentIds) {
		CoursesDto coursesDto = new CoursesDto();
		List<Student> students = studentRepository.findAllByIdIn(studentIds);
		Optional<Courses> courses = coursesRepository.findById(courseId);
		if (courses.isPresent()) {
			List<StudentCourse> studentCourses = new ArrayList<StudentCourse>();
			for (Student student : students) {
				StudentCourse studentCourse = StudentCourse.builder().courses(courses.get()).student(student)
						.status("ENROLLED").build();
				studentCourses.add(studentCourse);
			}
			studentCourseRepository.saveAll(studentCourses);
			BeanUtils.copyProperties(courses, coursesDto);
		} else {
			throw new BadRequestException("Course Not Found");
		}
		return coursesDto;
	}

	@Override
	public CoursesDto updateCourse(CourseUpdateDto coursesDto) {
		Optional<Courses> courses = coursesRepository.findById(coursesDto.getId());
		CoursesDto coursesDto2 = new CoursesDto();
		if (courses.isPresent()) {
			Courses courses2 = courses.get();
			BeanUtils.copyProperties(coursesDto, courses2);
			coursesRepository.save(courses2);
			BeanUtils.copyProperties(courses2, coursesDto2);
		}
		return coursesDto2;
	}
}
