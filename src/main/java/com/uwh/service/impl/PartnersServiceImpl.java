package com.uwh.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uwh.api.response.PartnersDto;
import com.uwh.domain.Partners;
import com.uwh.repositories.OrganizationRepository;
import com.uwh.repositories.PartnersRepository;
import com.uwh.service.PartnersService;
import com.uwh.utils.DtoConverter;

/**
 * Product service implement.
 */
@Service
public class PartnersServiceImpl implements PartnersService {

	@Autowired
	private PartnersRepository partnersRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	public List<PartnersDto> findAllPartners() throws IllegalAccessException, InvocationTargetException {
		return DtoConverter.convertToPartnerDto(partnersRepository.findAll());
	}

	@Override
	public PartnersDto savePartner(PartnersDto partnersDto) {
		Partners partners = Partners.builder()
				.address(partnersDto.getAddress())
				.email(partnersDto.getEmail())
				.organization(organizationRepository.findById(partnersDto.getOrganization().getId()).get())
				.partnerName(partnersDto.getPartnerName())
				.phone1(partnersDto.getPhone1())
				.phone2(partnersDto.getPhone2())
				.build();
		partners = partnersRepository.save(partners);
		partnersDto.setId(partners.getId());
		return partnersDto;
	}

	@Override
	public PartnersDto updatePartner(PartnersDto partnersDto) {
		PartnersDto dto= new PartnersDto();
		Optional<Partners> optional = partnersRepository.findById(partnersDto.getId());
		if (optional.isPresent()) {
			BeanUtils.copyProperties(partnersDto, optional.get());
			partnersRepository.save(optional.get());
			BeanUtils.copyProperties(optional.get(), dto);
		}
		return dto;
	}

}
