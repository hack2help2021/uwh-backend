package com.uwh.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uwh.api.response.OrganizationDto;
import com.uwh.domain.Organization;
import com.uwh.repositories.OrganizationRepository;
import com.uwh.service.OrganizationService;

@Service
public class OrganizationServiceImpl implements OrganizationService {
	
	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	public List<OrganizationDto> getOrganizations() {
		List<Organization> organizations = organizationRepository.findAll();
		List<OrganizationDto> organizationDtos = new ArrayList<OrganizationDto>();
		for (Organization organization : organizations) {
			OrganizationDto organizationDto = new OrganizationDto();
			BeanUtils.copyProperties(organization, organizationDto);
			organizationDtos.add(organizationDto);
		}
		return organizationDtos;
	}
}
