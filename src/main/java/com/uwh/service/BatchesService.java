package com.uwh.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.uwh.api.response.BatchesDTO;

public interface BatchesService {
	
	List<BatchesDTO> findAllBatchByCourseId(Long couserId) throws IllegalAccessException, InvocationTargetException;

    BatchesDTO saveBatch(BatchesDTO batchesDTO) throws IllegalAccessException, InvocationTargetException;

}
