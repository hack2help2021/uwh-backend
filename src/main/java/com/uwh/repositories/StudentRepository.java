package com.uwh.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uwh.domain.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
	List<Student> findAllByIdIn(List<Long> studentIds);
	public Optional<Student> findByRoleAndUidAndUidTypeAndPassword(String role,String uid,String uidType,String password);
}
