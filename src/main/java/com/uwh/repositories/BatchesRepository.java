package com.uwh.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uwh.domain.Batches;
import com.uwh.domain.Courses;

@Repository
public interface BatchesRepository extends JpaRepository<Batches,Long> {
	
	List<Batches> findAllByCoursesId(Long partnerId);

}
