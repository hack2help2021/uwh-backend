package com.uwh.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.uwh.domain.Batches;
import com.uwh.domain.Courses;
import com.uwh.domain.Student;

@Repository
public interface CountStudentRepository extends JpaRepository<Student,Long> {
	
	@Query(value = "SELECT count(id) FROM Student")
	public int countStudents();
}

