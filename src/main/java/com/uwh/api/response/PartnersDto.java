package com.uwh.api.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ramakrishna Veldandi
 * @project UWH
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartnersDto {
    private Long id;
    private String partnerName;
    private String address;
    private String email;
    private String phone1;
    private String phone2;
    private OrganizationDto organization;
}
