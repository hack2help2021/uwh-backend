package com.uwh.api.response;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uwh.domain.Organization;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentsDetailsDto {
	
    private Long id;
    private String uid;
    private String uidType;
    private String regdNo;
    private String studentName;
    private String gender;
    private String phone;
    private String additionalPhone;
    private String bplCard;
    private String regdDate;
    private String purpose;//Dropdown: College Placement Assistance/Hunar Course And Job/
    private String dateOfBirth;
    
    private String qualification;
    private String additionalCourse;
    private String university;
    private Integer experience;
    private String field;
    private Double currentSalary;
    private String studentCurrentSkills;
    private String studentInterestedSkills;
    private String studentInterestedJobFields;
    private String studentJobLocationPreferences;
    
    
    private Double expectedJobSalary;
    
    private String currentAddress;
    private String currentCity;
    private String districtName;
    private String pinCode;
            
    private String email;
    private String phone1;
    private String whatsAppNo2;
    
    private String referredByType; //drop down -> Self, Sponsored By - Honur/Colleague/FB/Friends/Other/Marketing Channel
    private String referredByName;
    
    private String courseName;
    private String status;
    private String partnerName;
    private String orgName ;
    private String jobStatus;
    private String jobName;
    private String jobType;
    private String jobLocation;
    private String jobDescription;
    private Double maxSalary;
}
