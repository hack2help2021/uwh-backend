package com.uwh.api.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BatchesDTO {
	
	
    private Long id;
    private String startDate;
    private String endDate;
    private String location;
    private CoursesDto courses;
    private List<StudentDto> students;

}
