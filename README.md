### uwh-backend
Backend Service for UWH Hack2help hackathon 2021
### To run this boilerplate code , you need to have MySQL database setup locally. /database/springbootdb20210822.sql is used to have the required file.
In application.properties , the DB query string is used to connect the database.

### Swagger URL to check the API locally is http://localhost:9000/swagger-ui/
